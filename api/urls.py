from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'tales', views.TaleViewSet)
router.register(r'profiles', views.ProfileViewSet)
router.register(r'comments', views.CommentViewSet)
router.register(r'replies', views.ReplyViewSet)

# addiottionally, we include login urls for the browsable API
urlpatterns = [
    path('', include(router.urls)),
    path('auth/login/', views.LoginView.as_view(), name='auth_login'),
    path('auth/logout/', views.LogoutView.as_view(), name='auth_logout'),
    path('auth/signup/', views.SignupView.as_view(), name='auth_signup'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
