from email import contentmanager
from statistics import mode
from django.contrib.auth.models import AbstractUser
from django.db import models
class CustomUser(AbstractUser):
    email = models.EmailField(max_length=254, unique=True)
    created_at = models.DateField(auto_now_add=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'password']
class Profile(models.Model):
    user = models.OneToOneField(CustomUser, to_field='username', on_delete=models.CASCADE)
    profile_name = models.CharField(max_length=30, default='My Profile Name')
    signature = models.CharField(max_length=30)
    description = models.CharField(max_length=280)
    image = models.CharField(max_length=254, default='https://cdn-icons.flaticon.com/png/512/2102/premium/2102647.png?token=exp=1646669481~hmac=9de6a3dbc750cc0974be80cfe34324f8')
    followers = models.IntegerField(default=0)
    following = models.IntegerField(default=0)

    def __str__(self):
        return self.profile_name

class Tale(models.Model):
    profile_id = models.ForeignKey(Profile, on_delete=models.CASCADE)
    title = models.CharField(max_length=30)
    text = models.TextField(max_length=2000)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

class Reply(models.Model):
    comment_id = models.ForeignKey('Comment', related_name="replies", on_delete=models.CASCADE)
    profile_id = models.ForeignKey(Profile, on_delete=models.CASCADE)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.content[:10]

class Comment(models.Model):
    tale_id = models.ForeignKey(Tale, on_delete=models.CASCADE)
    profile_id = models.ForeignKey(Profile, on_delete=models.CASCADE)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.content[:10]

