from django.contrib import admin
from .models import CustomUser, Tale, Profile, Comment, Reply

# Register your models here.
admin.site.register(CustomUser)
admin.site.register(Tale)
admin.site.register(Profile)
admin.site.register(Comment)
admin.site.register(Reply)
