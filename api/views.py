from django.shortcuts import render
from rest_framework import viewsets, filters, status, generics
from .serializers import TaleSerializer, UserSerializer, ProfileSerializer, CommentSerializer, ReplySerializer
from .models import Tale, Profile, Comment, Reply
from django_filters.rest_framework import DjangoFilterBackend
from django.contrib.auth import authenticate, login, logout
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth.hashers import make_password


class TaleViewSet(viewsets.ModelViewSet):
    queryset = Tale.objects.all().order_by('title')
    serializer_class = TaleSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter]
    filterset_fields = ['id','profile_id','title','text','created_at']
    search_fields = ['id','profile_id','title','text','created_at']
    ordering_fields = ['id','profile_id','title','text','created_at']

class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all().order_by('profile_name')
    serializer_class = ProfileSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['id','user','profile_name','signature','description','image','followers','following']


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all().order_by('id')
    serializer_class = CommentSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['id','tale_id','profile_id','content','created_at']

    def get_queryset(self):
        comment = Comment.objects.all()
        return comment

    def create(self, request, *args,**kwargs):
        comment_data = request.data
        new_comment = Comment.objects.create(
            profile_id=Profile.objects.get(id=comment_data["profile_id"]),
            tale_id = Tale.objects.get(id=comment_data["tale_id"]),
            content = comment_data["content"]
            )
        new_comment.save()
        serializer = CommentSerializer(new_comment)
        return Response(serializer.data)

class ReplyViewSet(viewsets.ModelViewSet):
    queryset = Reply.objects.all().order_by('id')
    serializer_class = ReplySerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['id','comment_id','profile_id','content','created_at']

    def get_queryset(self):
        reply = Reply.objects.all()
        return reply

    def create(self, request, *args,**kwargs):
        reply_data = request.data
        new_reply = Reply.objects.create(
            profile_id=Profile.objects.get(id=reply_data["profile_id"]),
            comment_id = Comment.objects.get(id=reply_data["comment_id"]),
            content = reply_data["content"]
            )
        new_reply.save()
        serializer = ReplySerializer(new_reply)
        return Response(serializer.data)

# AUTHENTICATIONS VIEWS
class LoginView(APIView):
    def post(self, request):
        # Recuperamos las credenciales y autenticamos al usuario
        email = request.data.get('email', None)
        password = request.data.get('password', None)
        user = authenticate(email=email, password=password)

        # Si es correcto añadimos a la request la información de sesión
        if user:
            login(request, user)
            return Response(
    UserSerializer(user).data,
    status=status.HTTP_200_OK)

        # Si no es correcto devolvemos un error en la petición
        return Response(
            status=status.HTTP_404_NOT_FOUND)


class LogoutView(APIView):
    def post(self, request):
        # Borramos de la request la información de sesión
        logout(request)

        # Devolvemos la respuesta al cliente
        return Response(status=status.HTTP_200_OK)

class SignupView(generics.CreateAPIView):
    serializer_class = UserSerializer

    def validate_password(self, value):
        return make_password(value)
