from rest_framework import serializers
from .models import CustomUser, Reply, Tale, Profile, Comment
from rest_framework import serializers
from django.contrib.auth import get_user_model


class TaleSerializer(serializers.HyperlinkedModelSerializer):
    profile_id = serializers.PrimaryKeyRelatedField(queryset = Profile.objects.all())
    class Meta:
        model = Tale
        fields = ('id', 'profile_id', 'title', 'text', 'created_at')


class ProfileSerializer(serializers.HyperlinkedModelSerializer):    
    class Meta:
        model = Profile
        fields = ('id', 'user', 'profile_name', 'signature',
                  'description', 'image', 'followers', 'following')


# authentications
class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True)
    username = serializers.CharField(
        required=True)
    password = serializers.CharField(
        min_length=6, write_only=True)

    def create(self, validated_data):
        user = CustomUser.objects.create(
            email=validated_data['email'],
            username=validated_data['username'],
        )

        user.set_password(validated_data['password'])
        user.save()
        return user


    class Meta:
        model = get_user_model()
        fields = ('email', 'username', 'password')

class ReplySerializer(serializers.ModelSerializer):
    comment_id = serializers.PrimaryKeyRelatedField(queryset = Comment.objects.all())
    '''profile_id = serializers.PrimaryKeyRelatedField(queryset = Profile.objects.all())'''
    class Meta:
        model = Reply
        fields = ('id','comment_id','profile_id','content','created_at')
        depth=1
class CommentSerializer(serializers.ModelSerializer):
    tale_id = serializers.PrimaryKeyRelatedField(queryset = Tale.objects.all())
    ''''profile_id = serializers.PrimaryKeyRelatedField(queryset = Profile.objects.all())'''
    replies = ReplySerializer(many=True, read_only=True)
    class Meta:
        model = Comment
        fields = ('id','tale_id','profile_id','content','replies','created_at')
        depth = 1


